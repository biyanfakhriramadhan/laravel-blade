@extends('layouts.master')
   
@section('content')

@section('header', 'Halaman Home')

   <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">halaman home</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <h1>Media Online</h1>
    <h3>Sosial Media Developer</h3>
    <p>Belajar dan Berbagi agar hidup menjadi lebih baik</p>
    <h4>Benefit Join di Media Online</h4>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h4>Cara Bergabung ke Media Online</h4>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di <a href="{{url('/register')}}">Form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        Footer
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
  @endsection